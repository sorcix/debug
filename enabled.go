// +build debug

package debug

import (
	"log"
	"os"
)

const Enabled bool = true

var logger *log.Logger = log.New(os.Stderr, "DEBUG: ", log.Ldate|log.Ltime)

// Print writes a line to Stderr. Arguments are handled in the manner of fmt.Print.
func Print(a ...interface{}) {
	logger.Print(a)
}

// Printf writes a line to Stderr. Arguments are handled in the manner of fmt.Printf.
func Printf(format string, a ...interface{}) {
	logger.Printf(format, a...)
}

// Println writes a line to Stderr. Arguments are handled in the manner of fmt.Println.
func Println(a ...interface{}) {
	logger.Println(a...)
}
