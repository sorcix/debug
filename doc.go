// Package debug provides functions for debug output.
//
// When building the application with the "debug" tag, messages sent using the
// functions in this package will be written to Stderr. In case you don't specify
// the debug tag, the functions are empty and will be removed by the compiler.
package debug // import "go.sorcix.com/debug"
