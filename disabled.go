// +build !debug

package debug

const Enabled bool = false

func Print(a ...interface{}) {}

func Printf(format string, a ...interface{}) {}

func Println(a ...interface{}) {}
